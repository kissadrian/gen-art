import { Flex } from "@chakra-ui/react";
import Menu from "./components/menu/Menu";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Constellation from "./components/constellation/Constellation";
import AnimatedCloud from "./components/animated-cloud/AnimatedCloud";
import TextAnimation from "./components/text-animation/TextAnimation";
import Orbit from "./components/orbit/Orbit";

function App() {
  return (
    <Flex w={"100%"} justify={"center"}>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Menu />} />
          <Route path="/constellation" element={<Constellation />} />
          <Route path="/animated-cloud" element={<AnimatedCloud />} />
          <Route path="/text-animation" element={<TextAnimation />} />
          <Route path="/orbit" element={<Orbit />} />
        </Routes>
      </BrowserRouter>
    </Flex>
  );
}

export default App;
