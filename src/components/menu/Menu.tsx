import { Flex } from "@chakra-ui/react";
import { SectionType } from "../../types/types";
import SectionContainer from "./SectionContainer";

const sections: SectionType[] = [
  {
    title: "Particle effects",
    sectionPhases: [
      { title: "Constellation effect", liveDemoLink: "/constellation" },
      { title: "Animated Cloud", liveDemoLink: "/animated-cloud" },
      { title: "Text Animation", liveDemoLink: "/text-animation" },
      { title: "Orbit effect", liveDemoLink: "/orbit" },
      { title: "Sunrays Effect", liveDemoLink: "/something" },
      { title: "Image animation", liveDemoLink: "/something" },
    ],
  },
  {
    title: "Chaos effects",
    sectionPhases: [
      { title: "Rainbow lightnings", liveDemoLink: "/something" },
      {
        title: "Starts with Symmetry, Ends in Chaos",
        liveDemoLink: "/something",
      },
      { title: "Chaotic paintbrush", liveDemoLink: "/something" },
      { title: "Phase 4", liveDemoLink: "/something" },
      { title: "Phase 5", liveDemoLink: "/something" },
    ],
  },
  {
    title: "Fractals",
    sectionPhases: [{ title: "Phase 1", liveDemoLink: "/something" }],
  },
  {
    title: "ASCII Art",
    sectionPhases: [{ title: "Phase 1", liveDemoLink: "/something" }],
  },
];

const Menu = () => {
  const menuContent = sections.map((s, index) => (
    <SectionContainer section={s} key={index} />
  ));

  return (
    <Flex direction="column" gap={"40px"} w="70%" p={5}>
      {menuContent}
    </Flex>
  );
};

export default Menu;
