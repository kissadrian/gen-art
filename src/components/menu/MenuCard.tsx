import { Box, Flex, GridItem, Text } from "@chakra-ui/react";
import React from "react";
import { Link } from "react-router-dom";

type Props = {
  title: string;
  liveDemoLink: string;
};

const SectionCard: React.FC<Props> = ({ title, liveDemoLink }) => {
  return (
    <GridItem h="10" bg={"white"} minHeight={"150px"} borderRadius={"10px"}>
      <Flex
        p={4}
        h={"100%"}
        justify={"center"}
        align={"center"}
        direction={"column"}
        justifyContent={"space-between"}
      >
        <Text fontSize={"larger"} fontWeight={"bold"}>
          {title}
        </Text>
        <Flex gap={5}>
          <Link to={liveDemoLink}>
            <Box
              p={1}
              _hover={{
                color: "white",
                backgroundColor: "orange",
                cursor: "pointer",
              }}
              borderRadius={5}
            >
              Live Demo
            </Box>
          </Link>
          <Box
            p={1}
            _hover={{
              color: "white",
              backgroundColor: "orange",
              cursor: "pointer",
            }}
            borderRadius={5}
          >
            Source Code
          </Box>
        </Flex>
      </Flex>
    </GridItem>
  );
};

export default SectionCard;
