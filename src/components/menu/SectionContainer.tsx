import { Flex, Grid, Text } from "@chakra-ui/react";
import React from "react";
import SectionCard from "./MenuCard";
import { SectionType } from "../../types/types";

type Props = {
  section: SectionType;
};

const SectionContainer: React.FC<Props> = ({ section }) => {
  const innerContent = section.sectionPhases.map((s,index) => (
    <SectionCard liveDemoLink={s.liveDemoLink} title={s.title} key={index}/>
  ));

  return (
    <Flex direction="column">
      <Flex w={"100%"} justifyContent={"center"}>
      <Text fontSize="3xl" fontWeight={"bold"}>{section.title}</Text>
      </Flex>
      <Grid templateColumns="repeat(3, 1fr)" gap={6} w={"100%"} p={4}>
        {innerContent}
      </Grid>
    </Flex>
  );
};

export default SectionContainer;
