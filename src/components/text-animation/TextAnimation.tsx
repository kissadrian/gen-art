import { useEffect, useRef, useState } from "react";
import { TextAnimationEffect } from "../../utils/text-animation/TextAnimationEffect";

const TextAnimation = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [windowSize, setWindowSize] = useState<{
    width: number;
    height: number;
  }>({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  useEffect(() => {
    const canvas = canvasRef.current;
    if (!canvas) {
      return;
    }
    canvas.width = windowSize.width;
    canvas.height = windowSize.height;

    const e = new TextAnimationEffect(canvas);
    e.animate();

    window.addEventListener("resize", handleWindowResize);

    return () => {
      window.removeEventListener("resize", handleWindowResize);
      e.cleanup();
    };
  }, [windowSize,canvasRef]);

  const handleWindowResize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  return <canvas ref={canvasRef} id="text-animation-canvas" />;
};

export default TextAnimation;
