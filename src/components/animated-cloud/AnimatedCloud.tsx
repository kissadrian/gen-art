import { useEffect, useRef, useState } from "react";
import { AnimatedCloudEffect } from "../../utils/animated-cloud/AnimatedCloudEffect";

const AnimatedCloud = () => {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [windowSize, setWindowSize] = useState<{
    width: number;
    height: number;
  }>({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas?.getContext("2d");
    if (!ctx || !canvas) {
      return;
    }
    canvas.width = windowSize.width;
    canvas.height = windowSize.height;

    const e = new AnimatedCloudEffect(canvas);
    e.animate();

    window.addEventListener("resize", handleWindowResize);

    return () => {
      window.removeEventListener("resize", handleWindowResize);
      e.cleanup();
    };
  }, [windowSize]);

  const handleWindowResize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  return <canvas ref={canvasRef} id="animated-cloud-canvas" />;
};

export default AnimatedCloud;
