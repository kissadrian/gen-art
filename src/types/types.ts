export interface SectionType{
    title: string
    sectionPhases: SectionPart[]
}

export interface SectionPart {
    title: string
    liveDemoLink: string
}