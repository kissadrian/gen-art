import { Particle } from "../Particle";
import { ParticleGenerator } from "./ParticleGenerator";

export class ConstellationEffect {
  particles: Particle[];
  windowWidth: number;
  windowHeight: number;
  ctx: CanvasRenderingContext2D;
  particleGenerator: ParticleGenerator;
  maxDistance: number;

  constructor(canvas: HTMLCanvasElement) {
    this.ctx = canvas.getContext("2d")!;
    this.windowWidth = canvas.width;
    this.windowHeight = canvas.height;
    this.particleGenerator = new ParticleGenerator(
      canvas.width,
      canvas.height,
      2,
      15,
      40
    );
    this.particles = this.particleGenerator.generateParticles(60);
    this.maxDistance = 300;
    this.animate = this.animate.bind(this);
  }

  updateParticles() {
    this.particles.forEach((p) => this.moveParticle(p));
  }

  moveParticle(p: Particle) {
    if (
      p.center.x + p.vx > this.windowWidth - p.radius ||
      p.center.x + p.vx < p.radius
    ) {
      p.vx *= -1;
    }
    if (
      p.center.y + p.vy > this.windowHeight - p.radius ||
      p.center.y + p.vy < p.radius
    ) {
      p.vy *= -1;
    }
    p.center.x += p.vx;
    p.center.y += p.vy;
  }

  drawLines() {
    this.ctx.lineWidth = 2;

    for (let i = 0; i < this.particles.length - 1; i++) {
      for (let j = i + 1; j < this.particles.length; j++) {
        const distance = this.particles[i].center.distanceFrom(
          this.particles[j].center
        );

        if (distance < this.maxDistance) {
          const opacity = 1 - distance / this.maxDistance;
          this.ctx.strokeStyle = `rgba(0, 0, 0, ${opacity})`;
          this.ctx.beginPath();
          this.ctx.moveTo(
            this.particles[i].center.x,
            this.particles[i].center.y
          );
          this.ctx.lineTo(
            this.particles[j].center.x,
            this.particles[j].center.y
          );
          this.ctx.stroke();
        }
      }
    }
  }

  drawParticles() {
    this.particles.forEach((p) => {
      p.draw(this.ctx);
    });
  }

  animate() {
    this.ctx.clearRect(0, 0, this.windowWidth, this.windowHeight);
    this.ctx.fillStyle = "white";
    this.ctx.fillRect(0, 0, this.windowWidth, this.windowHeight);
    this.ctx.lineWidth = 2;

    this.updateParticles();
    this.drawLines();
    this.drawParticles();
    requestAnimationFrame(this.animate);
  }
}
