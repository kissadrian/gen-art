import { Particle } from "../Particle";
import { Point } from "../Point";

export class ParticleGenerator {
  maxX: number;
  maxY: number;
  minRadius: number;
  maxRadius: number;
  baseSpeed: number;

  constructor(
    maxX: number,
    maxY: number,
    baseSpeed: number,
    minRadius: number,
    maxRadius: number
  ) {
    this.maxX = maxX;
    this.maxY = maxY;
    this.minRadius = minRadius;
    this.maxRadius = maxRadius;
    this.baseSpeed = baseSpeed;
  }

  generateParticles(n: number): Particle[] {
    const particles = [];
    for (let i = 0; i < n; i++) {
      const radius =
        this.minRadius +
        Math.floor(Math.random() * (this.maxRadius - this.minRadius));
      let vx = (1 - radius / this.maxRadius) * this.baseSpeed;
      let vy = (1 - radius / this.maxRadius) * this.baseSpeed;
      vx = Math.random() > 0.5 ? -vx : vx;
      vy = Math.random() > 0.5 ? -vy : vy;

      const p = new Particle(
        new Point(
          Math.floor(radius + Math.random() * (this.maxX - 2 * radius)),
          Math.floor(radius + Math.random() * (this.maxY - 2 * radius))
        ),
        radius,
        vx,
        vy,
        "white",
        "black"
      );
      particles.push(p);
    }

    return particles;
  }
}
