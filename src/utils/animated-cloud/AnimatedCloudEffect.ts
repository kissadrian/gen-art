import { Particle } from "../Particle";
import { Point } from "../Point";
import { ParticleGenerator } from "./ParticleGenerator";

export class AnimatedCloudEffect {
  particles: Particle[];
  windowWidth: number;
  windowHeight: number;
  ctx: CanvasRenderingContext2D;
  particleGenerator: ParticleGenerator;
  maxRadius: number;
  radiusGrowth: number;
  radiusReduction: number;
  mouseProperties: {
    x?: number;
    y?: number;
    effectRadius: number;
  };
  frameCounter: number;

  constructor(canvas: HTMLCanvasElement) {
    this.ctx = canvas.getContext("2d")!;
    this.windowWidth = canvas.width;
    this.windowHeight = canvas.height;
    this.maxRadius = 50;
    this.particleGenerator = new ParticleGenerator(
      this.windowWidth,
      this.windowHeight,
      this.maxRadius
    );
    this.particles = this.particleGenerator.generateParticles(1000);
    this.mouseProperties = { effectRadius: 60 };
    this.radiusGrowth = 15;
    this.radiusReduction = 1;
    this.frameCounter = 0;
    this.animate = this.animate.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    window.addEventListener("mousemove", this.handleMouseMove);
  }

  cleanup() {
    window.removeEventListener("mousemove", this.handleMouseMove);
  }

  handleMouseMove(e: MouseEvent) {
    this.mouseProperties = {
      ...this.mouseProperties,
      x: e.clientX,
      y: e.clientY,
    };
  }

  drawParticles() {
    this.particles.forEach((p) => {
      this.updateParticleRadius(p);
      this.moveParticle(p);
      p.draw(this.ctx);
    });
  }

  updateParticleRadius(p: Particle) {
    if (
      this.mouseProperties.x &&
      this.mouseProperties.y &&
      p.center.distanceFrom(
        new Point(this.mouseProperties.x, this.mouseProperties.y)
      ) <= this.mouseProperties.effectRadius
    ) {
      p.radius =
        p.radius + this.radiusGrowth > this.maxRadius
          ? this.maxRadius
          : p.radius + this.radiusGrowth;
    } else {
      p.radius =
        p.radius - this.radiusReduction >= 0
          ? p.radius - this.radiusReduction
          : 0;
    }
  }

  moveParticle(p: Particle) {
    if (
      p.center.x + p.vx > this.windowWidth - this.maxRadius ||
      p.center.x + p.vx < this.maxRadius
    ) {
      p.vx *= -1;
    }
    if (
      p.center.y + p.vy > this.windowHeight - this.maxRadius ||
      p.center.y + p.vy < this.maxRadius
    ) {
      p.vy *= -1;
    }
    p.center.x += p.vx;
    p.center.y += p.vy;
  }

  animate() {
    if (this.frameCounter === 600) {
      this.mouseProperties = {
        ...this.mouseProperties,
        x: undefined,
        y: undefined,
      };
      this.frameCounter = 0;
    }

    this.ctx.clearRect(0, 0, this.windowWidth, this.windowHeight);
    this.drawParticles();
    this.frameCounter++;
    requestAnimationFrame(this.animate);
  }
}
