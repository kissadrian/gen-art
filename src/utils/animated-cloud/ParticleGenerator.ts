import { Particle } from "../Particle";
import { Point } from "../Point";

export class ParticleGenerator {
  maxX: number;
  maxY: number;
  maxRadius: number;
  colors: string[];

  constructor(maxX: number, maxY: number, maxRadius: number) {
    this.maxX = maxX;
    this.maxY = maxY;
    this.maxRadius = maxRadius;
    this.colors = [
      "white",
      "rgba(255,255,255,0.3)",
      "rgba(173,216,230,0.8)",
      "rgba(211,211,211,0.8)",
    ];
  }

  generateParticles(n: number): Particle[] {
    const particles = [];
    for (let i = 0; i < n; i++) {
      const radius = 0;
      let vx = 0.1 + Math.random()*0.3
      let vy = 0.1 + Math.random()*0.3
      vx = Math.random() > 0.5 ? -vx : vx;
      vy = Math.random() > 0.5 ? -vy : vy;
      const color = this.colors[Math.floor(Math.random() * this.colors.length)];

      const p = new Particle(
        new Point(
          Math.floor(
            this.maxRadius + Math.random() * (this.maxX - 2 * this.maxRadius)
          ),
          Math.floor(
            this.maxRadius + Math.random() * (this.maxY - 2 * this.maxRadius)
          )
        ),
        radius,
        vx,
        vy,
        color,
        "transparent"
      );
      particles.push(p);
    }

    return particles;
  }
}
