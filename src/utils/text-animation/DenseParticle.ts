import { Point } from "../Point";

export class DenseParticle {
  baseCenter: Point;
  center: Point;
  radius: number;
  vx: number;
  vy: number;
  fillStyle: string;
  strokeStyle: string;
  density: number;

  constructor(
    point: Point,
    radius: number,
    vx: number,
    vy: number,
    fillStyle: string,
    strokeStyle: string,
    density: number
  ) {
    this.strokeStyle = strokeStyle;
    this.fillStyle = fillStyle;
    this.baseCenter = point;
    this.center = new Point(point.x, point.y);
    this.radius = radius;
    this.vx = vx;
    this.vy = vy;
    this.density = density;
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.save();

    ctx.strokeStyle = this.strokeStyle;
    ctx.beginPath();
    ctx.fillStyle = this.fillStyle;
    ctx.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();

    ctx.restore();
  }
}
