import { Particle } from "../Particle";
import { Point } from "../Point";
import { DenseParticle } from "./DenseParticle";
import { ParticleGenerator } from "./ParticleGenerator";

export class TextAnimationEffect {
  ctx: CanvasRenderingContext2D;
  windowWidth: number;
  windowHeight: number;
  textImageWidth: number;
  textImageHeight: number;
  particleGenerator: ParticleGenerator;
  particles: DenseParticle[];
  mouseProperties: {
    x?: number;
    y?: number;
    effectRadius: number;
  };
  frameCounter: number;
  resetTreshold: number;

  constructor(canvas: HTMLCanvasElement) {
    this.ctx = canvas.getContext("2d")!;
    this.windowHeight = canvas.height;
    this.windowWidth = canvas.width;
    this.textImageHeight = 30;
    this.textImageWidth = 100;
    this.particleGenerator = new ParticleGenerator(
      this.windowWidth,
      this.windowHeight,
      this.textImageWidth,
      this.textImageHeight
    );
    this.resetTreshold = 0.5;
    this.particles = this.particleGenerator.generateParticles(
      this.generateImageData()
    );
    this.mouseProperties = {
      effectRadius: 100,
    };
    this.frameCounter = 0;

    this.animate = this.animate.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    window.addEventListener("mousemove", this.handleMouseMove);
  }

  generateImageData(): ImageData {
    this.ctx.font = `${this.textImageHeight}px Verdana`;
    this.ctx.fillStyle = "white";
    this.ctx.fillText("Hover!", 0, this.textImageHeight);
    return this.ctx.getImageData(
      0,
      0,
      this.textImageWidth,
      this.textImageHeight
    );
  }

  handleMouseMove(e: MouseEvent) {
    this.mouseProperties = {
      ...this.mouseProperties,
      x: e.clientX,
      y: e.clientY,
    };
  }

  cleanup() {
    window.removeEventListener("mousemove", this.handleMouseMove);
  }

  drawParticles() {
    this.particles.forEach((p) => {
      this.updateVelocities(p);
      this.moveParticle(p);
      p.draw(this.ctx);
    });
  }

  updateVelocities(p: DenseParticle) {
    if (this.mouseProperties.x && this.mouseProperties.y) {
      const mousePos = new Point(
        this.mouseProperties.x,
        this.mouseProperties.y
      );
      const distance = mousePos.distanceFrom(p.center);

      if (distance <= this.mouseProperties.effectRadius) {
        this.pushFurther(
          p,
          mousePos,
          (distance) => 1 - distance / this.mouseProperties.effectRadius
        );
      } else {
        if (p.baseCenter.distanceFrom(p.center) <= this.resetTreshold) {
          this.resetParticle(p);
        } else if (p.baseCenter.distanceFrom(p.center) !== 0) {
          this.pullCloser(p, p.baseCenter);
        }
      }
    } else {
      if (p.baseCenter.distanceFrom(p.center) <= this.resetTreshold) {
        this.resetParticle(p);
      } else if (p.baseCenter.distanceFrom(p.center) !== 0) {
        this.pullCloser(p, p.baseCenter);
      }
    }
  }

  pushFurther(
    particle: DenseParticle,
    point: Point,
    force: (distance: number) => number
  ) {
    const dy = particle.center.y - point.y;
    const dx = particle.center.x - point.x;
    const angle = Math.atan2(dy, dx);
    const distance = point.distanceFrom(particle.center);

    particle.vx = force(distance) * particle.density * Math.cos(angle);
    particle.vy = force(distance) * particle.density * Math.sin(angle);
  }

  pullCloser(particle: DenseParticle, point: Point) {
    const dy = particle.center.y - point.y;
    const dx = particle.center.x - point.x;
    particle.vx = -dx / 20;
    particle.vy = -dy / 20;
  }

  resetParticle(particle: DenseParticle) {
    particle.center.x = particle.baseCenter.x;
    particle.center.y = particle.baseCenter.y;
    particle.vx = 0;
    particle.vy = 0;
  }

  moveParticle(p: Particle) {
    p.center.x += p.vx;
    p.center.y += p.vy;

    if (p.center.x < p.radius) {
      p.center.x = p.radius;
    } else if (p.center.x > this.windowWidth - p.radius) {
      p.center.x = this.windowWidth - p.radius;
    }

    if (p.center.y < p.radius) {
      p.center.y = p.radius;
    } else if (p.center.y > this.windowHeight - p.radius) {
      p.center.y = this.windowHeight - p.radius;
    }
  }

  animate() {
    if (this.frameCounter === 300) {
      this.mouseProperties = {
        ...this.mouseProperties,
        x: undefined,
        y: undefined,
      };
      this.frameCounter = 0;
    }
    this.ctx.clearRect(0, 0, this.windowWidth, this.windowHeight);
    this.ctx.fillStyle = "black";
    this.ctx.fillRect(0, 0, this.windowWidth, this.windowHeight);
    this.drawParticles();
    this.frameCounter++;
    requestAnimationFrame(this.animate);
  }
}
