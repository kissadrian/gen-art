import { Point } from "../Point";
import { DenseParticle } from "./DenseParticle";

export class ParticleGenerator {
  scaleFactor: number;
  windowWidth: number;
  windowHeight: number;
  mappedWidth: number;
  mappedHeight: number;

  constructor(
    windowWidth: number,
    windowHeight: number,
    mappedWidth: number,
    mappedHeight: number
  ) {
    this.windowWidth = windowWidth;
    this.windowHeight = windowHeight;
    this.mappedWidth = mappedWidth;
    this.mappedHeight = mappedHeight;
    this.scaleFactor = this.calculateScalingFactor();
    console.log(this.scaleFactor);
  }

  generateParticles(imageData: ImageData): DenseParticle[] {
    const radius = 3;
    const particles: DenseParticle[] = [];
    const pixelArray = imageData.data;

    const offsetX = this.calculateOffset(
      this.mappedWidth * this.scaleFactor,
      this.windowWidth
    );
    const offsetY = this.calculateOffset(
      this.mappedHeight * this.scaleFactor,
      this.windowHeight
    );

    for (let j = 0; j < imageData.height; j++) {
      for (let i = 0; i < imageData.width; i++) {
        const index = (j * imageData.width + i) * 4;
        const R = pixelArray[index];
        const G = pixelArray[index + 1];
        const B = pixelArray[index + 2];
        const A = pixelArray[index + 3] / 255;

        if (R > 128) {
          const p = new DenseParticle(
            new Point(
              offsetX + i * this.scaleFactor,
              offsetY + j * this.scaleFactor
            ),
            radius,
            0,
            0,
            `rgba(${R},${G},${B},${A})`,
            "transparent",
            20 + Math.floor(Math.random() * 6)
          );
          particles.push(p);
        }
      }
    }

    return particles;
  }

  calculateScalingFactor(): number {
    return Math.min(
      Math.floor(this.windowWidth / this.mappedWidth),
      Math.floor(this.windowHeight / this.mappedHeight)
    );
  }

  calculateOffset(l: number, fullLength: number): number {
    console.log(l, fullLength);
    return Math.floor((fullLength - l) / 2);
  }
}
