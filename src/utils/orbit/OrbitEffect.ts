import { Point } from "../Point";
import { OrbitParticle } from "./OrbitParticle";
import { OrbitParticleGenerator } from "./OrbitParticleGenerator";

export class OrbitEffect {
  particles: OrbitParticle[];
  windowWidth: number;
  windowHeight: number;
  ctx: CanvasRenderingContext2D;
  particleGenerator: OrbitParticleGenerator;
  resetTreshold: number;
  orbitRadius: number;
  mouseProperties: {
    x?: number;
    y?: number;
    effectRadius: number;
    pressedDown: boolean;
  };
  img: HTMLImageElement;

  constructor(canvas: HTMLCanvasElement) {
    this.windowWidth = canvas.width;
    this.windowHeight = canvas.height;
    this.ctx = canvas.getContext("2d")!;
    this.mouseProperties = {
      effectRadius: 50,
      pressedDown: false,
    };
    this.orbitRadius = 10;
    this.particleGenerator = new OrbitParticleGenerator(
      2,
      this.windowWidth,
      this.windowHeight
    );
    this.particles = [];
    this.resetTreshold = 0.5;
    this.img = new Image();
    this.img.src = "homepage-docker-logo.png";
    this.img.onload = () => {
      this.ctx.drawImage(this.img, 0, 0, this.img.width, this.img.height);
      this.particles = this.particleGenerator.generateParticlesFromImageData(
        this.ctx.getImageData(0, 0, this.img.width, this.img.height)
      );
    };

    this.handleMouseDown = this.handleMouseDown.bind(this);
    this.handleMouseUp = this.handleMouseUp.bind(this);
    this.handleMouseMove = this.handleMouseMove.bind(this);
    this.animate = this.animate.bind(this);

    window.addEventListener("mousemove", this.handleMouseMove);
    window.addEventListener("mousedown", this.handleMouseDown);
    window.addEventListener("mouseup", this.handleMouseUp);
  }

  handleMouseMove(e: MouseEvent) {
    this.mouseProperties = {
      ...this.mouseProperties,
      x: e.clientX,
      y: e.clientY,
    };
  }

  handleMouseDown() {
    this.mouseProperties = {
      ...this.mouseProperties,
      pressedDown: true,
    };
  }

  handleMouseUp() {
    this.mouseProperties = {
      ...this.mouseProperties,
      pressedDown: false,
    };
  }

  cleanup() {
    window.removeEventListener("mousedown", this.handleMouseDown);
    window.removeEventListener("mouseup", this.handleMouseUp);
    window.removeEventListener("mousemove", this.handleMouseMove);
  }

  drawParticles() {
    this.particles.forEach((p) => {
      this.updateMovement(p);
      p.draw(this.ctx);
    });
  }

  updateMovement(p: OrbitParticle) {
    if (p.angle && this.mouseProperties.pressedDown) {
      this.moveOnOrbit(p);
    } else if (
      this.mouseProperties.x &&
      this.mouseProperties.y &&
      this.mouseProperties.pressedDown
    ) {
      const distance = Math.round(
        p.center.distanceFrom(
          new Point(this.mouseProperties.x, this.mouseProperties.y)
        )
      );

      if (distance <= this.orbitRadius) {
        this.moveOnOrbit(p);
      } else if (
        distance > this.orbitRadius &&
        distance < this.mouseProperties.effectRadius
      ) {
        this.pullCloserToMouse(p, distance);
      } else {
        this.moveParticleToOriginalPos(p);
      }
    } else if (!this.mouseProperties.pressedDown) {
      p.angle = undefined;
      const distance = p.center.distanceFrom(p.originalCenter);
      if (distance < this.resetTreshold) {
        this.resetParticle(p);
      } else {
        this.moveParticleToOriginalPos(p);
      }
    }
  }

  resetParticle(p: OrbitParticle) {
    p.center.x = p.originalCenter.x;
    p.center.y = p.originalCenter.y;
  }

  moveParticleToOriginalPos(p: OrbitParticle) {
    const dy = p.center.y - p.originalCenter.y;
    const dx = p.center.x - p.originalCenter.x;

    p.center.x -= dx / 20;
    p.center.y -= dy / 20;
  }

  moveOnOrbit(p: OrbitParticle) {
    if (!this.mouseProperties.x || !this.mouseProperties.y) {
      return;
    }

    if (!p.angle) {
      const dy = p.center.y - this.mouseProperties.y;
      const dx = p.center.x - this.mouseProperties.x;
      p.angle = Math.atan2(dy, dx);
    }

    p.center.x = this.mouseProperties.x + this.orbitRadius * Math.cos(p.angle);
    p.center.y = this.mouseProperties.y + this.orbitRadius * Math.sin(p.angle);
    p.angle += p.va;
  }

  pullCloserToMouse(p: OrbitParticle, distance: number) {
    if (!this.mouseProperties.x || !this.mouseProperties.y) {
      return;
    }

    const dy = p.center.y - this.mouseProperties.y;
    const dx = p.center.x - this.mouseProperties.x;
    const angle = Math.atan2(dy, dx);

    p.center.x -=
      Math.cos(angle) *
      Math.floor(5 + Math.random() * 2) *
      (distance / this.mouseProperties.effectRadius);
    p.center.y -=
      Math.sin(angle) *
      Math.floor(5 + Math.random() * 2) *
      (distance / this.mouseProperties.effectRadius);
  }

  animate() {
    this.ctx.clearRect(0, 0, this.windowWidth, this.windowHeight);
    this.drawParticles();

    requestAnimationFrame(this.animate);
  }
}
