import { Point } from "../Point";
import { OrbitParticle } from "./OrbitParticle";

export class OrbitParticleGenerator {
  radius: number;
  windowWidth: number;
  windowHeight: number;
  minAngleSpeed: number;
  maxAngleSpeed: number;

  constructor(radius: number, windowWidth: number, windowHeight: number) {
    this.radius = radius;
    this.windowWidth = windowWidth;
    this.windowHeight = windowHeight;
    this.minAngleSpeed = 0.5;
    this.maxAngleSpeed = 0.7;
  }

  generateParticles(n: number): OrbitParticle[] {
    const particles = [];

    for (let i = 0; i < n; i++) {
      particles.push(
        new OrbitParticle(
          new Point(
            this.radius + Math.random() * (this.windowWidth - 2 * this.radius),
            this.radius + Math.random() * (this.windowHeight - 2 * this.radius)
          ),
          this.radius,
          this.minAngleSpeed +
            Math.random() * (this.maxAngleSpeed - this.minAngleSpeed),
          `hsl(${Math.floor(Math.random() * 360)},100%,50%)`,
          "transparent"
        )
      );
    }

    return particles;
  }

  generateParticlesFromImageData(img: ImageData): OrbitParticle[] {
    const particles = [];

    for (let j = 0; j < img.height; j += 2*this.radius) {
      for (let i = 0; i < img.width; i += 2*this.radius) {
        const index = (j * img.width + i) * 4;
        const R = img.data[index];
        const G = img.data[index + 1];
        const B = img.data[index + 2];
        const A = img.data[index + 3] / 255;

        if (A > 0.5) {
          console.log(i, j);
          particles.push(
            new OrbitParticle(
              new Point(i, j),
              this.radius,
              this.minAngleSpeed +
                Math.random() * (this.maxAngleSpeed - this.minAngleSpeed),
              `rgba(${R},${G},${B},${A})`,
              "transparent"
            )
          );
        }
      }
    }
    console.log(particles);
    return particles;
  }
}
