import { Point } from "../Point";

export class OrbitParticle {
  originalCenter: Point;
  center: Point;
  radius: number;
  fillStyle: string;
  strokeStyle: string;
  va: number;
  angle?: number;
  rotationAngle?: number;

  constructor(
    point: Point,
    radius: number,
    va: number,
    fillStyle: string,
    strokeStyle: string
  ) {
    this.originalCenter = new Point(point.x, point.y);
    this.center = point;
    this.strokeStyle = strokeStyle;
    this.fillStyle = fillStyle;
    this.radius = radius;
    this.va = va;
    this.angle = undefined;
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.save();

    ctx.strokeStyle = this.strokeStyle;
    ctx.beginPath();
    ctx.fillStyle = this.fillStyle;
    ctx.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();

    ctx.restore();
  }
}
