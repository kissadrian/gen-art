export class Point {
  x: number;
  y: number;

  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }

  distanceFrom(other: Point): number{
    return Math.hypot(other.x - this.x, other.y - this.y);
  }
}
