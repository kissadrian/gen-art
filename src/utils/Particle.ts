import { Point } from "./Point";

export class Particle {
  center: Point;
  radius: number;
  vx: number;
  vy: number;
  fillStyle: string;
  strokeStyle: string;

  constructor(
    point: Point,
    radius: number,
    vx: number,
    vy: number,
    fillStyle: string,
    strokeStyle: string
  ) {
    this.strokeStyle = strokeStyle;
    this.fillStyle = fillStyle;
    this.center = point;
    this.radius = radius;
    this.vx = vx;
    this.vy = vy;
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.save()

    ctx.strokeStyle = this.strokeStyle;
    ctx.beginPath();
    ctx.fillStyle = this.fillStyle;
    ctx.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
    ctx.stroke();
    
    ctx.restore()
  }
}
