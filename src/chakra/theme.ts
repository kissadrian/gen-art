import { extendTheme } from "@chakra-ui/react";

const defaultTheme = extendTheme({
  styles: {
    global: {
      body: {
        bg: "gray.300",
      },
    },
  },
});

export default defaultTheme;
