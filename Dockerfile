FROM node:18.12.1-alpine as builder

WORKDIR /usr/src/app

COPY package*.json .

RUN --mount=type=cache,target=/usr/src/app/.npm \
  npm set cache /usr/src/app/.npm && \
  npm ci

COPY . .

RUN npm run build

###
FROM nginx:1.22-alpine

COPY --from=builder /usr/src/app/dist /usr/share/nginx/html

EXPOSE 80
